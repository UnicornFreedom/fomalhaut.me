/**
 * An ever programming version of myself :3
 */

var TYPING = 0, MISTYPING = 1, BACKSPACING = 2;

var MAX_LINES = 5;               // how long will be the visible part of a listing
var lines = 0;                   // how long it is right now
var listing = ". . . . . .";     // the full listing itself
var printed = "";                // the visible part of it
var position = 0;                // cursor position
var ERRORS_RATE = 0.1;           // the probability of typing wrong word
var error_position = -1;         // some random word from the listing, printed 'by error'
var state = TYPING;              // do 'I' write the code or undo some errors
var language = "lua";            // current language (for syntax highlighting)
var autoplay = true;             // do 'I' need to open a new file after the current one ends

// emulation container
var scroller = document.getElementById("scroller");


// hacked up way to emulate `Enter`
function rewind() {
    if (lines < MAX_LINES) lines++;
    else printed = printed.substring(printed.indexOf("\n") + 1);
}

function isLetter(char) {
    return (char >= 'a' && char <= 'z') || (char >= 'A' && char <= 'Z')
}

// search for random word in the listing (for 'errors' emulation)
function findErrorPosition() {
    var position = Math.round(Math.random() * listing.length);
    // adjust to the beginning of the next word
    while (position < listing.length && isLetter(listing[position])) position++;
    while (position < listing.length && !isLetter(listing[position])) position++;
    //
    if (position < listing.length)
        return position;
    else
        return -1;
}

function ongoingChar() {
    return listing[ state === MISTYPING ? error_position : position ]
}
function atBeginningOfWord() {
    return isLetter(ongoingChar()) && !isLetter(printed.slice(-1));
}
function atEndOfWord() {
    return !isLetter(ongoingChar()) && isLetter(printed.slice(-1));
}

// do yet another letter
function type() {
    // what am I thinking of?
    if (state === TYPING && atBeginningOfWord() && Math.random() < ERRORS_RATE) {
        error_position = findErrorPosition();
        if (error_position !== -1) state = MISTYPING;
    } else if (state === MISTYPING && atEndOfWord()) {
        state = BACKSPACING;
    } else if (state === BACKSPACING && atBeginningOfWord()) {
        state = TYPING;
    }
    // type
    if (state === BACKSPACING)
        printed = printed.slice(0, -1);
    else {
        printed += ongoingChar();
        if (ongoingChar() === '\n') rewind();
    }
    // update HTML
    scroller.innerHTML = hljs.highlight(language, printed, true).value;
    // move pointer
    if (state === TYPING) position++;
    if (state === MISTYPING) error_position++;
    if (position >= listing.length) {
        if (autoplay) {
            rewind();
            printed += "\n";
            position = 0;
            fetch();
        } else {
            console.log("Autoplay is turned off.");
        }
    } else if (error_position >= listing.length) {
        state = BACKSPACING;
    } else {
        var timeout = Math.random() * (state === BACKSPACING ? 180 : (atEndOfWord() ? 450 : 60));
        setTimeout(type, timeout);
    }
}


// try and guess the language of given source code file by the file extension
function extensionToLanguage(extension) {
    if (hljs.getLanguage(extension) !== undefined) return extension;
    else
        switch (extension) {
            case "kt": return "kotlin";
            case "rb": return "ruby";
            case "sbt": return "scala";
            case "rs": return "rust";
            case "png": case "jpg": case "jpeg": case "bmp": case "ico":
            case "jar": case "ttf": case "woff": case "lock": return undefined;
            default: return "ini";
        }
}

function httpGetAsync(url, callback) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200)
            callback(xmlHttp.responseText);
    };
    xmlHttp.open("GET", url, true); // true for asynchronous
    xmlHttp.send(null);
}

// the maximum is exclusive and the minimum is inclusive
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

// get random file listing
function fetch() {
    console.log("* * *")
    var amount = github.repos.length + gitlab.projects.length
    if (getRandomInt(1, amount + 1) <= github.repos.length) {
        fetchGitHub();
    } else {
        fetchGitLab();
    }
}

// GitHub interface
var github = {
    repos: []
}

// get random file listing from GitHub
function fetchGitHub() {
    var repo = github.repos[Math.floor(Math.random() * github.repos.length)];
    console.log("Repo: https://github.com/MoonlightOwl/" + repo.name);
    httpGetAsync("https://api.github.com/repos/MoonlightOwl/" + repo.name + "/git/trees/master?recursive=1", function(data) {
        var tree = JSON.parse(data).tree;
        var files = tree.filter(function(item) { return item.type === "blob"; });
        var file = files[getRandomInt(0, files.length)];
        var url = "https://raw.githubusercontent.com/MoonlightOwl/" + repo.name + "/master/" + file.path;
        fetchListing(url, file.path);
    });
}

// GitLab interface
var gitlab = {
    projects: []
}

// get random file listing from GitLab
function fetchGitLab() {
    var project = gitlab.projects[Math.floor(Math.random() * gitlab.projects.length)];
    console.log("Repo: " + project.web_url);
    httpGetAsync("https://gitlab.com/api/v4/projects/" + project.id + "/repository/tree?recursive=true", function(data) {
        var tree = JSON.parse(data);
        var files = tree.filter(function(item) { return item.type === "blob"; });
        var file = files[getRandomInt(0, files.length)];
        var url = "https://relay.fomalhaut.me/?url=https://gitlab.com/UnicornFreedom/" + project.name + "/raw/master/" + file.path;
        fetchListing(url, file.path);
    });
}

function fetchListing(url, filename) {
    console.log("File: " + url);
    httpGetAsync(url, function (data) {
        var parts = filename.split('.');
        language = extensionToLanguage(parts.pop());
        console.log("Language" + (parts.length > 1 ? " (autodetected): " : ": ") + language);
        if (language === undefined){
            if (autoplay) {
                console.log("Switching to another file...");
                fetch();
            } else {
                console.log("Cannot determine the file language, and autoplay is off.");
            }
        } else {
            listing = data;
            setTimeout(type, Math.random() * 500);
        }
    })
}

// init
console.log("Initializing...");

// check if some specific file was requested with URL arguments
var url = new URL(window.location.href);
var file = url.searchParams.get("data");
var max = url.searchParams.get("lines");
var lang = url.searchParams.get("lang");

if (max != null) {
    console.log(max);
    MAX_LINES = Number(max) - 1;
    console.log(MAX_LINES);
}

// in case of file - print this file and nothing else
if (file != null) {
    autoplay = false;
    console.log("Done");
    fetchListing(file, lang ? lang : file);
}
// else, run the common routine
else {
  // get info about my GitHub repos
  httpGetAsync("https://api.github.com/users/MoonlightOwl/repos", function(data) {
      github.repos = JSON.parse(data);
      // get info about GitLab repos
      httpGetAsync("https://gitlab.com/api/v4/users/UnicornFreedom/projects", function(data) {
          gitlab.projects = JSON.parse(data);
          // get some code to type
          console.log("Done");
          fetch();
      });
});
}
